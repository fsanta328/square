﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class GPGSS
{
	//public MPLobbyListener m_lobbyListener;
	private static GPGSS m_instance = null;
    public string m_message = null;

	private GPGSS ()
	{
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate ();
	}

	public static GPGSS Instance 
	{
		get 
		{
			if (m_instance == null) 
			{
				m_instance = new GPGSS();
			}
			return m_instance;
		}
	}

	public string SignIn() 
	{
		if (! PlayGamesPlatform.Instance.localUser.authenticated) 
		{
			PlayGamesPlatform.Instance.localUser.Authenticate((bool success) => 
				{
					if (success) 
					{
                        m_message = "We're signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName + ".";

                        //ShowMPStatus ("We're signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName);
                        //TextStatus("We're signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName);

                    }

					else
					{
                        m_message = "Player is not signed in.";

                        //ShowMPStatus("Player is not signed in");
                        //TextStatus("player is not signed in.");
					}
				});
            return m_message;
        } 

		else 
		{
            m_message = "You're already signed in as " + PlayGamesPlatform.Instance.localUser.userName +".";

            //ShowMPStatus ("You're already signed in.");
            //TextStatus("You're already signed in.");
		}
        return m_message;
	}

	public string TrySilentSignIn() 
	{
		if (! PlayGamesPlatform.Instance.localUser.authenticated) 
		{
			PlayGamesPlatform.Instance.Authenticate ((bool success) => 
				{
					if (success) 
					{
                        m_message = "Silently signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName;
                        //ShowMPStatus ("Silently signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName);
						//Debug.Log ("Silently signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName);
					} 

					else 
					{
                        //m_message = "silent sign in failed.";
                        m_message = "-";
                        //ShowMPStatus ("silent sign in failed.");
						//Debug.Log ("silent sign in failed.");
					}
				}, true);
		} 

		else 
		{
            m_message = "-";

            //ShowMPStatus ("already silent sign in");
			//Debug.Log("already silent sign in");
		}
        return m_message;
	}

	public bool IsAuthenticated() 
	{
		return PlayGamesPlatform.Instance.localUser.authenticated;
	}

	public void SignOut() 
	{
		//ShowMPStatus ("User signed out");
		PlayGamesPlatform.Instance.SignOut ();
	}

    public string TextStatus(string message)
    {
        return message;
    }

	//public void ShowMPStatus(string message)
	//{
	//	if (m_lobbyListener != null) 
	//	{
	//		m_lobbyListener.SetLobbyStatusMessage(message);
	//	}
	//}

	//public void ShowLeaderboards()
	//{
		//ShowMPStatus("displaying leaderboards");
		//((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (); // Show current (Active) leaderboard
	//}

	//public void ShowLeaderboard (string a_leaderboardID)
	//{
	//	((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (a_leaderboardID);
			
		//GPGSS.m_instance.ShowLeaderboards (a_leaderboardID);
	//}
}

//public interface MPLobbyListener 
//{
//	void SetLobbyStatusMessage(string message);
//	void HideLobby();
//}
