﻿using UnityEngine;

public interface IDamageable
{
	void TakeHit (float a_damage, Vector3 a_hitPoint, Vector3 a_hitDirection);

	void TakeDamage (float a_damage);
}
