﻿using UnityEngine;
using System.Collections;

public class LivingEntity : MonoBehaviour , IDamageable
{
	public float m_startHealth;
	public float m_health { get; protected set; }
	protected bool m_dead;

	public event System.Action OnDeath;

	protected virtual void Start()
	{
		m_health = m_startHealth;
	}

	public virtual void TakeHit (float a_damage, Vector3 a_hitPoint, Vector3 a_hitDirection)
	{
		TakeDamage (a_damage);
	}

	public virtual void TakeDamage (float a_damage)
	{
		m_health -= a_damage;

		if (m_health <= 0  /*&& !m_dead*/) 
		{
			Die ();
		}
	}

	//[ContextMenu("Self Destruct")]
	public virtual void Die()
	{
		//m_dead = true;
		if (OnDeath != null) 
		{
			OnDeath ();
		}
	}
}
