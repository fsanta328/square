﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(NavMeshAgent))]
public class Enemy : LivingEntity
{
	public enum State
	{
		Idle,
		Chasing,
		Attacking
	};

	public ParticleSystem m_deathEffect;
	public static event System.Action m_onDeathStatic;

	State m_currentState;
	NavMeshAgent m_pathfinder;
	Transform m_target;
	LivingEntity m_targetEntity;
	Material m_skinMaterial;
    Color m_originalColor;
    //int m_iD;

	float m_attackDistance = .5f;
	float m_timeBetweenAttack = 1f;
	float m_damage = 1;

	float nextAttackTime;
	float m_myCollisionRadius;
	float m_targetCollisionRadius;

	bool m_hasTarget;

	void Awake()
	{
		m_pathfinder = GetComponent <NavMeshAgent> ();

		if (GameObject.FindGameObjectWithTag ("Player") != null)
		{
			m_hasTarget = true;

			m_target = GameObject.FindGameObjectWithTag ("Player").transform;
			m_targetEntity = m_target.GetComponent <LivingEntity> ();

			m_myCollisionRadius = GetComponent <CapsuleCollider> ().radius;
			m_targetCollisionRadius = m_target.GetComponent <CapsuleCollider> ().radius;
		}
	}

	protected override void Start () 
	{
		base.Start ();

		if (m_hasTarget)
		{
			m_currentState = State.Chasing;
			m_targetEntity.OnDeath += OnTargetDeath;
			StartCoroutine (UpdatePath ());
		}
	}

    void OnEnable()
    {
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            m_hasTarget = true;
            if (m_hasTarget)
            {
                m_currentState = State.Chasing;
                m_targetEntity.OnDeath += OnTargetDeath;
                StartCoroutine(UpdatePath());
            }
        }
    }

	public void SetCharacteristics (float a_moveSpeed, float a_hitsToKillPlayer, float a_enemyHealth, Color a_skinColor)
	{
		m_pathfinder.speed = a_moveSpeed;
        //Debug.Log("hp =" + a_enemyHealth);
		if (m_hasTarget) 
		{
            //m_damage = 1;
			m_damage = Mathf.Ceil(m_targetEntity.m_startHealth / a_hitsToKillPlayer);
		}
		m_startHealth = a_enemyHealth;

		m_deathEffect.startColor = new Color (a_skinColor.r, a_skinColor.g, a_skinColor.b, 1);
		m_skinMaterial = GetComponent <Renderer> ().material;
		m_skinMaterial.color = a_skinColor;
		m_originalColor = m_skinMaterial.color;
	}

	public override void TakeHit (float a_damage, Vector3 a_hitPoint, Vector3 a_hitDirection)
	{
		AudioManager.m_aInstance.PlaySound ("Impact", transform.position);
		if (a_damage >= m_health)
		{
            StopCoroutine(UpdatePath());
            if (m_onDeathStatic != null) 
			{
				m_onDeathStatic ();
                //Debug.Log("ddeath static");
			}
			AudioManager.m_aInstance.PlaySound ("Enemy Death", transform.position);
			Destroy (Instantiate(m_deathEffect.gameObject, a_hitPoint, Quaternion.FromToRotation(Vector3.forward, a_hitDirection)) as GameObject, m_deathEffect.startLifetime);
		}
		base.TakeHit (a_damage, a_hitPoint, a_hitDirection);
	}

	void OnTargetDeath()
	{
		m_hasTarget = false;
		m_currentState = State.Idle;
	}

	void Update () 
	{
		if(m_hasTarget)
		{
            if (Time.time > nextAttackTime)
            {
                float sqrDistanceToTarget = (m_target.position - transform.position).sqrMagnitude;

                if (sqrDistanceToTarget < Mathf.Pow(m_attackDistance + m_myCollisionRadius + m_targetCollisionRadius, 2))
                {
                    nextAttackTime = Time.time + m_timeBetweenAttack;
                    AudioManager.m_aInstance.PlaySound("Enemy Attack", transform.position);
                    StartCoroutine(Attack());
                }
            }
        }

        //else
        //{
        //    gameObject.SetActive(false);
        //}
    }

    IEnumerator Attack()
	{
		m_currentState = State.Attacking;
		m_pathfinder.enabled = false;
		Vector3 originalPos = transform.position;

		Vector3 dirToTarget = (m_target.position - transform.position).normalized;
		Vector3 attackPosition = m_target.position - dirToTarget * (m_myCollisionRadius);

		float attackSpeed = 3;
		float percent = 0;

		m_skinMaterial.color = Color.red;
		bool hasAppliedDamage = false;

		while (percent <= 1) 
		{
			if (percent >= .5f && !hasAppliedDamage)
			{
				hasAppliedDamage = true;
				m_targetEntity.TakeDamage (m_damage);
			}
			percent += Time.deltaTime * attackSpeed;
			float interpolation = (-Mathf.Pow (percent, 2) + percent) * 4;
			transform.position = Vector3.Lerp (originalPos, attackPosition, interpolation);

			yield return null;
		}
		m_skinMaterial.color = m_originalColor;
		m_currentState = State.Chasing;
		m_pathfinder.enabled = true;
	}

	IEnumerator UpdatePath()
	{
		float refreshRate = .50f;

		while (m_hasTarget) 
		{
			if (m_currentState == State.Chasing)
			{
				Vector3 dirToTarget = (m_target.position - transform.position).normalized;
				Vector3 targetPosition = m_target.position - dirToTarget * (m_myCollisionRadius + m_targetCollisionRadius +m_attackDistance/2);
				if (!m_dead) 
				{
					m_pathfinder.SetDestination (targetPosition);
				}
			}
			yield return new WaitForSeconds (refreshRate);
		}
	}

    public override void Die()
    {
        m_pathfinder.enabled = true;
        base.Die();
        int aIndex = PoolManager.instance.m_spawnedObjects.IndexOf(this.gameObject);

        PoolManager.instance.RemoveFromActive(this.gameObject, aIndex);
        gameObject.SetActive(false);
    }
}
