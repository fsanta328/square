﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour
{
	public Image m_fadePlane;
	public GameObject m_gameOverUI;

	[Header("Banner Info")]
	public RectTransform m_newWaveBanner;
	public Text m_newWaveTitle;
	public Text m_newWaveEnemyCount;

	[Header("ScoreUI")]
	public Text m_scoreUI;
	public Text m_gameOverScoreUI;

	public RectTransform m_healthBar;
	public GameObject m_gameUIHolder;
	public GameObject m_PauseMenuHolder;
	public Text m_ammoCount;

	Spawner m_spawner;
	Player m_player;

	//NEW
    float healthPercent = 0;
    private float m_sScore = 0;
    public Slider[] m_volumeSliders;
    public static bool m_paused = false;
    string m_ammoText;
    int m_signInPref;
    MapGenerator m_map;

    void Awake()
	{
		m_spawner = FindObjectOfType<Spawner> ();
        m_map = FindObjectOfType<MapGenerator> ();
		m_spawner.OnNewWave += OnNewWave;
	}

	void Start () 
	{
		FindObjectOfType<Player> ().OnDeath += OnGameOver;
		m_player = FindObjectOfType<Player> ();

        if (AudioManager.m_aInstance != null)
        {
            m_volumeSliders[0].value = AudioManager.m_aInstance.m_masterVolumePercent;
            m_volumeSliders[1].value = AudioManager.m_aInstance.m_musicVolumePercent;
            m_volumeSliders[2].value = AudioManager.m_aInstance.m_sfxVolumerPercent;
        }
        m_signInPref = GameManager.m_Instance.m_logInPref;
    }

	void Update()
	{
        if (m_sScore != Score.m_score)
        {
            m_scoreUI.text = Score.m_score.ToString("D6");
            m_sScore = Score.m_score;
        }

        if (m_player != null)
        {
            healthPercent = m_player.m_health / m_player.m_startHealth;
            m_healthBar.localScale = new Vector3(healthPercent, 1, 1);
        }
    }

    public void ReloadGun()
    {
        FindObjectOfType<Gun>().Reload();
        //Debug.Log("reloading");
    }

    public void UpdateAmmo (string a_string)
    {
        m_ammoText = a_string;
        m_ammoCount.text = m_ammoText;
    }

	void OnNewWave (int a_waveNumber)
	{
		string[] numbers = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten" };
		m_newWaveTitle.text = "- Wave : " + numbers [a_waveNumber - 1] + " -";
		string enemyCountString = ((m_spawner.m_waves [a_waveNumber - 1].m_infinite) ? "Infinite" : m_spawner.m_waves [a_waveNumber - 1].m_enemyCount + "");
		m_newWaveEnemyCount.text = "Enemies: " + enemyCountString;
		StopCoroutine ("AnimateNewWaveBanner");
		StartCoroutine ("AnimateNewWaveBanner");
	}

	IEnumerator AnimateNewWaveBanner()
	{
		float delayTime = 2f;
		float speed = 1f;
		float animationPercent = 0;
		int dir = 1;
		float endDelayTime = Time.time + 1 / speed + delayTime;

		while (animationPercent >= 0) 
		{
			animationPercent += Time.deltaTime * speed * dir;

			if (animationPercent >= 1)
			{
				animationPercent = 1;
				if (Time.time > endDelayTime) 
				{
					dir = -1;
				}
			}

			m_newWaveBanner.anchoredPosition = Vector2.up * Mathf.Lerp (0, 195, animationPercent);
			yield return null;
		}
	}

	void OnGameOver()
	{
        AudioManager.m_aInstance.StopMusic();
        m_gameUIHolder.SetActive(false);
        m_scoreUI.gameObject.SetActive(false);
        m_healthBar.transform.parent.gameObject.SetActive(false);
        m_map.StartCoroutine("MinimizeMap");
        int endKills = Score.m_kills;
        m_gameOverScoreUI.text = "Score: " + m_scoreUI.text + '\n' +
           "Kills: " + endKills;
        //StartCoroutine (Fade (Color.clear, new Color(0,0,0, 1f), 1));
        StartCoroutine("Wait");
        
	}

    IEnumerator Wait ()
    {
        yield return new WaitForSeconds(3.5f);
        if (m_signInPref == 1)
        {
            PostScore();
        }
        m_gameOverUI.SetActive(true);
    }

	IEnumerator Fade(Color a_from, Color a_to, float a_time)
	{
		float speed = 1 / a_time;
		float percent = 0;

		while (percent < 1) 
		{
			percent += Time.deltaTime * speed;
			m_fadePlane.color = Color.Lerp (a_from, a_to, percent);
			yield return null;
		}
        m_gameOverUI.SetActive(true);
    }

	//UI input
	public void StartNewGame ()
	{
		SceneManager.LoadScene (1);
	}

	public void ReturnToMainMenu ()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene (0);
	}

	public void PauseMenu ()
	{
        m_paused = true;
		m_gameUIHolder.SetActive (false);
		m_PauseMenuHolder.SetActive (true);
		Time.timeScale = 0;
	}

	public void Resume()
	{
        m_paused = false;
        Time.timeScale = 1;
		m_gameUIHolder.SetActive (true);
		m_PauseMenuHolder.SetActive (false);
	}

    public void SetMasterVolume(float a_volume)
    {
        AudioManager.m_aInstance.SetVolume(a_volume, AudioManager.AudioChannel.Master);
    }

    public void SetMusicVolume(float a_volume)
    {
        AudioManager.m_aInstance.SetVolume(a_volume, AudioManager.AudioChannel.Music);
    }

    public void SetSfxVolume(float a_volume)
    {
        AudioManager.m_aInstance.SetVolume(a_volume, AudioManager.AudioChannel.Sfx);
    }

    public void PostScore ()
	{
		int endScore = Score.m_score;
		int endKills = Score.m_kills;

        if (!GPGSS.Instance.IsAuthenticated()) 
		{
			GPGSS.Instance.SignIn ();
			if (!GPGSS.Instance.IsAuthenticated ())
			{
				//m_lobbyMessageText.text = "Unable to post to leaderboards, please try again.";
			}
		}

		else
		{
			//m_lobbyMessageText.text = "score = " + endScore + "\\n"+ "kills = " + endKills;
			Social.ReportScore (endScore, "CgkIlYDcuY4FEAIQAA", (bool success) => {
				// handle success or failure
			});

			Social.ReportScore (endKills, "CgkIlYDcuY4FEAIQCQ", (bool success) => {
				// handle success or failure
			});
		}
	}
}
