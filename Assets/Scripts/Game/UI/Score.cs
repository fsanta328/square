﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour 
{
	public static int m_score { get; private set;}
	public static int m_kills { get; private set;}
	float m_lastEnemyKillTime;
	int m_currentStreak = 1;
	float m_streakExpiriryTime = 1f;

	// Use this for initialization
	void Start () 
	{
		Enemy.m_onDeathStatic += OnEnemyKilled;
		FindObjectOfType<Player> ().OnDeath += OnPLayerDeath;
		m_score = 0;
		m_kills = 0;
	}

	void OnEnemyKilled()
	{
		if (Time.time < m_lastEnemyKillTime + m_streakExpiriryTime) 
		{
			m_currentStreak++;
		} 

		else
			m_currentStreak = 1;

		m_lastEnemyKillTime = Time.time;
		m_score += 5 + (int)Mathf.Pow (2, m_currentStreak);
		m_kills++;
	}

	void OnPLayerDeath()
	{
		Enemy.m_onDeathStatic -= OnEnemyKilled;
	}
}
