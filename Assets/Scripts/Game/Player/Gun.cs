﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour 
{
	public enum FireMode
	{
		Auto,
		Burst,
		Single
	}

	public FireMode m_fireMode;
	public Transform[] m_projectileSpawn;
	public Projectile m_projectile;
	public float m_fireRate = 100;
	public float m_muzzleVelocity = 35;
	public int m_magazineCapacity;

	[Header("Effects")]
	public Transform m_shell;
	public Transform m_shellejection;
	MuzzleFlash m_muzzleFlash;
	public AudioClip m_shootAudio;
	public AudioClip m_reloadAudio;

	//[Header("Recoil")]
	//public Vector2 m_recoilMinMax = new Vector3(.5f, .2f);
	//public Vector2 m_recoilAngleMinMax = new Vector3(3, 5);
	//public float m_recoilMoveRelaxTime = .1f;
	//public float m_recoilRotationRelaxTime =.1f;
	//Vector3 recoilSmoothDampVelocity;
	//float m_recoilAngle;
	//float m_recoilRotationSmoothDampVel;

	float m_nextShotTime;
	bool m_triggerReleasedSinceLastShot;
	public int m_burstCount;
	int m_shotsRemainingInBurst;
	public int m_remainingInMag;
	bool m_isReloading;
    public GameUI m_gameUI;
	//public float m_reloadTime;

	void Start()
	{
        m_gameUI = FindObjectOfType<GameUI>();
		m_shotsRemainingInBurst = m_burstCount;
		m_muzzleFlash = GetComponent<MuzzleFlash> ();
		m_remainingInMag = m_magazineCapacity;
        //new
        PoolManager.instance.CreatePool(m_projectile.gameObject, 8);
        PoolManager.instance.CreatePool(m_shell.gameObject, 8);
        m_gameUI.UpdateAmmo(m_remainingInMag + "");
    }

	void LateUpdate()
	{
		//transform.localPosition = Vector3.SmoothDamp (transform.localPosition, Vector3.zero, ref recoilSmoothDampVelocity, m_recoilMoveRelaxTime);
		//m_recoilAngle = Mathf.SmoothDamp (m_recoilAngle, 0, ref m_recoilRotationSmoothDampVel, m_recoilRotationRelaxTime);
		//transform.localEulerAngles = transform.localEulerAngles + Vector3.left * m_recoilAngle;

		if (!m_isReloading && m_remainingInMag == 0) 
		{
			Reload ();
		}
	}

	void Shoot()
	{
		//Debug.Log (Time.time + "time " + m_nextShotTime + " fire rate " + m_fireRate + "rate");
		//if not reloading, is within fire rate and has bullets in mag
		if ((!m_isReloading) && (Time.time > m_nextShotTime) && (m_remainingInMag > 0))
		{
			//shoot behaviours
			if (m_fireMode == FireMode.Burst)
			{
				if (m_shotsRemainingInBurst == 0) 
				{
					return;
				}
				m_shotsRemainingInBurst--;
			} 

			else if (m_fireMode == FireMode.Single) 
			{
				if (!m_triggerReleasedSinceLastShot) 
				{
					return;
				}
			}

			for (int i = 0; i < m_projectileSpawn.Length; i++) 
			{
				if (m_remainingInMag == 0) 
				{
					break;
				}
				m_remainingInMag--;
				m_nextShotTime = Time.time + m_fireRate / 100;

                //Projectile newProjectile = Instantiate (m_projectile, m_projectileSpawn[i].position, m_projectileSpawn[i].rotation) as Projectile;
                //newProjectile.SetSpeed (m_muzzleVelocity);
                Projectile m_projectileNew = PoolManager.instance.ReuseObject(m_projectile.gameObject).GetComponent<Projectile>();
                ReuseBullet(m_projectileNew, i);
			}
            GameObject a_shellNew = PoolManager.instance.ReuseObject(m_shell.gameObject);
            ReuseShell(a_shellNew);
			//Instantiate (m_shell, m_shellejection.position, m_shellejection.rotation);
			m_muzzleFlash.Activate ();
            //			//recoil
            //			transform.localPosition -= Vector3.forward * Random.Range(m_recoilMinMax.x, m_recoilMinMax.y);
            //			m_recoilAngle += Random.Range(m_recoilAngleMinMax.x, m_recoilAngleMinMax.y);
            //			m_recoilAngle = Mathf.Clamp (m_recoilAngle, 0, 30);
            //if (AudioManager.m_aInstance != null)
            //{
                AudioManager.m_aInstance.PlaySound(m_shootAudio, transform.position);
            //}
            m_gameUI.UpdateAmmo(m_remainingInMag + "");

        }
    }

    public void ReuseBullet(Projectile a_bullet, int a_indexA)
    {
        a_bullet.transform.position = m_projectileSpawn[a_indexA].position;
        a_bullet.transform.rotation = m_projectileSpawn[a_indexA].rotation;
        a_bullet.GetComponent<TrailRenderer>().Clear();
        a_bullet.gameObject.SetActive(true);
        a_bullet.SetSpeed(m_muzzleVelocity);
    }

    public void ReuseShell(GameObject a_shell)
    {
        a_shell.transform.position = m_shellejection.position;
        a_shell.transform.rotation = m_shellejection.rotation;
        a_shell.SetActive(true);
    }

    public void Reload()
	{
		if (!m_isReloading && m_remainingInMag != m_magazineCapacity) 
		{
			StartCoroutine (AnimateReload ());
			AudioManager.m_aInstance.PlaySound (m_reloadAudio, transform.position);
		}
	}

	IEnumerator AnimateReload()
	{
		m_isReloading = true;
		yield return new WaitForSeconds (.2f);

		float reloadSpeed = 1f / m_reloadAudio.length;
		float percent = 0;
		Vector3 m_initialRot = transform.localEulerAngles;
		float m_maxReloadAngle = 30;

		while (percent < 1)
		{
			percent += Time.deltaTime * reloadSpeed;
			float interpolation = (-Mathf.Pow (percent, 2) + percent) * 4;
			float reloadAngle = Mathf.Lerp (0, m_maxReloadAngle, interpolation);
			transform.localEulerAngles = m_initialRot + Vector3.left * reloadAngle;

			yield return null;
		}

		m_isReloading = false;
		m_remainingInMag = m_magazineCapacity;
        m_gameUI.UpdateAmmo(m_remainingInMag + "");

    }

    public void Aim(Vector3 a_point)
	{
		if (!m_isReloading) 
		{
			transform.LookAt (a_point);
		}
	}

	public void OnTriggerHold()
	{
		Shoot ();
		m_triggerReleasedSinceLastShot = false;
	}

	public void OnTriggerRelease()
	{
		m_triggerReleasedSinceLastShot = true;
		m_shotsRemainingInBurst = m_burstCount;
	}

}
