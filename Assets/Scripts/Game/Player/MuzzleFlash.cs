﻿using UnityEngine;
using System.Collections;

public class MuzzleFlash : MonoBehaviour 
{

	public GameObject m_flashHolder;
	public float m_flashTime;
	public Sprite[] m_flashSprites;
	public SpriteRenderer[] m_spriterenderers;

	void Start()
	{
		Deactivate ();
	}

	public void Activate()
	{
		m_flashHolder.SetActive (true);

		int flashSpriteIndex = Random.Range (0, m_flashSprites.Length);

		for (int i = 0; i < m_spriterenderers.Length; i++) 
		{
			m_spriterenderers [i].sprite = m_flashSprites [flashSpriteIndex];
		}

		Invoke ("Deactivate", m_flashTime);
	}

	void Deactivate()
	{
		m_flashHolder.SetActive (false);
	}
}
