﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class JoystickControl : MonoBehaviour , IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	private Image bgImg;
	private Image stickImg;
	public Vector3 inputDirection { set; get;}

	private void Start()
	{
		bgImg = GetComponent <Image> ();
		stickImg = transform.GetChild(0).GetComponent <Image> ();
		inputDirection = Vector3.zero;
	}

	public virtual void OnDrag (PointerEventData eventData)
	{
		Vector2 pos = Vector2.zero;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle 
			(bgImg.rectTransform, eventData.position, eventData.pressEventCamera, out pos)) 
		{
			pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
			pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

			float x = (bgImg.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
			float y = (bgImg.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

			inputDirection = new Vector3 (x, 0, y);
			inputDirection = (inputDirection.magnitude > 1) ? inputDirection.normalized : inputDirection;

			stickImg.rectTransform.anchoredPosition = new Vector3 (inputDirection.x * (bgImg.rectTransform.sizeDelta.x / 3),
				inputDirection.z * (bgImg.rectTransform.sizeDelta.y / 3));
		}	 
	}

	public virtual void OnPointerDown (PointerEventData eventData)
	{
		OnDrag (eventData);
	}

	public virtual void OnPointerUp (PointerEventData eventData)
	{
		stickImg.rectTransform.anchoredPosition = Vector3.zero;
		inputDirection = Vector3.zero;
	}

	public Vector3 GetPosition()
	{
		return inputDirection;
	}
}
