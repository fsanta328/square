﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour 
{
	public Transform m_weaponSlot;
	public Gun[] m_allGuns;
	public Gun m_equippedGun;

	public void EquipGun (Gun a_gunToEquip)
	{
		if (m_equippedGun != null)
			Destroy (m_equippedGun.gameObject);
		
		m_equippedGun = Instantiate (a_gunToEquip, m_weaponSlot.position, m_weaponSlot.rotation) as Gun;
		m_equippedGun.transform.parent = m_weaponSlot;
	}

	public void EquipGun (int a_weaponIndex)
	{
		EquipGun(m_allGuns[a_weaponIndex]);
	}

	public void OnTriggerHold()
	{
		if (m_equippedGun != null) 
		{
			m_equippedGun.OnTriggerHold ();
		}
	}

	public void OnTriggerRelease()
	{
		if (m_equippedGun != null) 
		{
			m_equippedGun.OnTriggerRelease ();
		}
	}

	public void Aim(Vector3 a_point)
	{
		if (m_equippedGun != null) 
		{
			m_equippedGun.Aim(a_point);
		}
	}

	public void Reload()
	{
		if (m_equippedGun != null) 
		{
			m_equippedGun.Reload();
		}
	}

	public float GunHeight {get {return m_weaponSlot.position.y;}}
}
