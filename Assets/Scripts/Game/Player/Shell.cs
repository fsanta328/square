﻿using UnityEngine;
using System.Collections;

public class Shell : MonoBehaviour 
{
	public Rigidbody m_myRigidbody;
	public float m_forceMin;
	public float m_forceMax;
	float m_lifeTime = 1.5f;

    void OnEnable()
    {
        //material = GetComponent<Renderer>().material;
        //material.color = Color.black;
        float force = Random.Range(m_forceMin, m_forceMax);
        m_myRigidbody.AddForce(transform.right * force);
        m_myRigidbody.AddTorque(Random.insideUnitSphere * force);
        StartCoroutine(Fade());
    }

	IEnumerator Fade()
	{
		yield return new WaitForSeconds (m_lifeTime);
		//float percent = 0;
		//float fadeSpeed = 1 / m_fadeTime;
		//Color initialColor = material.color;

		//while (percent < 1) 
		//{
		//	percent += Time.deltaTime * fadeSpeed;
		//	material.color = Color.Lerp (initialColor, Color.clear, percent);
		//	yield return null;
		//}
        gameObject.SetActive(false);
        //material.color = m_startColor;
        //Destroy (gameObject);
    }
}
