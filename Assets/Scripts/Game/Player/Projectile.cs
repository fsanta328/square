﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	float m_speed = 10;
	public LayerMask m_collisionMask;
	float m_damage = 1;
	public Color m_trailColor;
	//float m_lifetime = .75f;
	float m_skinWidth = .1f;

	void Start()
	{
		Collider[] initialCollisions = Physics.OverlapSphere (transform.position, .1f, m_collisionMask);

		if (initialCollisions.Length > 0) 
		{
			OnHitObject (initialCollisions[0], transform.position);
		}

		GetComponent<TrailRenderer> ().material.SetColor ("_TintColor", m_trailColor);
	}

    void OnEnable()
    {
        StartCoroutine (Lifetime());
    }

    IEnumerator Lifetime()
    {
        yield return new WaitForSeconds(.75f);
        gameObject.SetActive(false);
    }

	public void SetSpeed(float a_speed)
	{
		m_speed = a_speed;
	}
	
	// Update is called once per frame
	void Update () 
	{
		float moveDistance = m_speed * Time.deltaTime;
		CheckCollisions (moveDistance);
		transform.Translate (Vector3.forward * moveDistance);
	}

	void CheckCollisions (float moveDistance)
	{
		Ray ray = new Ray (transform.position, transform.forward);
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, moveDistance + m_skinWidth, m_collisionMask, QueryTriggerInteraction.Collide))
		{
			OnHitObject (hit.collider, hit.point);
		}
	}

	void OnHitObject(Collider a_collider, Vector3 a_hitPoint)
	{
		IDamageable damageableObject = a_collider.GetComponent <IDamageable> ();

		if (damageableObject != null) 
		{
			damageableObject.TakeHit (m_damage, a_hitPoint, transform.forward);
            gameObject.SetActive(false);
        }
	}

	void OnCollisionEnter (Collision a_collider)
	{
		if (a_collider.gameObject.tag == "Bullet")
			return;

        else
            gameObject.SetActive(false);
    }
}
//X  audiomanager update 
//X  projectile/shell start & update
//X  event systems.update
//X  when player dies audio listener is attached GC buildup
//X  spawn projectiles and shells from a pool

//X  game consistently lags
//X  music doubles if lose game to restart
//game ui update
//x  reduce health of player
//X  gun cliping into player  
//tutorial
//X  ammo count
//X  reload button
//enemies remaining
//power up, restore hp, shield
//graceful end screen/post score
//sniper clipping in player
//occasional lags???????
//bullets go through some obstacles
//use stringbuilder for ui text
//hide gpg stuff from non gpg users (leaderboards, achievements)