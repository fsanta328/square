﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour 
{
	Vector3 m_velocity;
	Rigidbody m_myRigidboy;

	void Start () 
	{
		m_myRigidboy = GetComponent <Rigidbody> ();
	}

	public void Move(Vector3 a_velocity)
	{
		m_velocity = a_velocity;
	}

	void FixedUpdate()
	{
		m_myRigidboy.MovePosition (m_myRigidboy.position + m_velocity * Time.fixedDeltaTime);
		//m_velocity = Vector3.zero;
	}

	public void LookAt (Vector3 a_lookPoint)
	{
		Vector3 heightCorrection = new Vector3 (a_lookPoint.x, transform.position.y, a_lookPoint.z);
		transform.LookAt (heightCorrection);
	}
}
