﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(GunController))]
public class Player : LivingEntity
{
	public float m_moveSpeed = 10;
	//Camera m_cameraPC;//reuse
	PlayerController m_controller;
	GunController m_gunController;
	public JoystickControl m_joystick;
	public JoystickControl m_lookStick;
    public AudioListener m_listener;
    public GameObject m_camera;
    private Vector3 m_newPos = new Vector3(0,12,0);

	//public Crosshairs m_crosshair;//reuse
	//public int m_ammo = 0;//reuse

    public bool DevMode = false;

    void Awake()
	{
		m_controller = GetComponent <PlayerController> ();
		m_gunController = GetComponent <GunController> ();
		//m_cameraPC = Camera.main;//reuse
		FindObjectOfType<Spawner> ().OnNewWave += OnNewWave;
	}

	protected override void Start () 
	{
		base.Start ();
        //if (DevMode)
        //{
        //    this.m_health = 50f;
        //}
	}

	void OnNewWave(int a_waveNumber)
	{
        m_camera.transform.position = new Vector3(0, 12, -5);
        m_health = m_startHealth;
		m_gunController.EquipGun (a_waveNumber - 1);
	}

    public void SlowDown()
    {
        Time.timeScale = 0.5f;
    }

    void Update()
    {
        //movement input mobile
        //MoveStick();
        //LookStick();

        for (int i = 0; i < Input.touchCount; i++)
        ////if (Input.GetMouseButtonDown(0))
        {
            Touch touch = Input.GetTouch(i);
            {
                if (touch.phase == TouchPhase.Began)
                {
                    MoveStick();
                    LookStick();
                    //Vector3 touchPos = new Vector3(touch.position.x, touch.position.y, 0);
                    //Vector3 mPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                    //if ((int)touchPos.x < Screen.width / 2)
                    //{
                    //Vector3 offset = new Vector3(120, 180, 0);
                    //m_leftStick.transform.position = touchPos - offset;
                    //}

                    //else
                    //{
                    //    //Debug.Log ("wider than half");
                    //    Vector3 offset = new Vector3(188 / 4, 188 / 4, 0);
                    //    m_rightStick.transform.position = touchPos - offset;
                    //}
                }

                if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Stationary)
                {
                    MoveStick();
                    LookStick();
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    m_controller.Move(Vector3.zero);
                }
            }
        }

        if (Input.touchCount == 0 && GameUI.m_paused == false)
            {
                SlowDown();
            }

            else if (GameUI.m_paused == false)
            {
                Time.timeScale = 1;
            }

            if (transform.position.y < -10)
            {
                TakeDamage(m_health);
            }

            //if (Input.GetKeyDown(KeyCode.Return))
            //{
            //    num+=2;
            //    m_gunController.EquipGun(num);
            //    Debug.Log(num);
            //}
            //      StandardLookStick ();
            //      //movement input PC
            //      Vector3 moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
            //      Vector3 moveVelocity = moveInput.normalized * m_moveSpeed;
            //      m_controller.Move(moveVelocity);
            //
            //      //look input PC
            //      Ray ray = m_cameraPC.ScreenPointToRay(Input.mousePosition);
            //      Plane groundPlane = new Plane(Vector3.up, Vector3.up * m_gunController.GunHeight);
            //      float rayDistance;
            //
            //      if (groundPlane.Raycast(ray, out rayDistance))
            //      {
            //          Vector3 point = ray.GetPoint(rayDistance);
            //          m_controller.LookAt(point);
            //          m_crosshair.transform.position = point;
            //          m_crosshair.DetectTarget(ray);
            //          if ((new Vector2(point.x, point.z) - new Vector2(transform.position.x, transform.position.z)).sqrMagnitude > 1)
            //          {
            //              m_gunController.Aim(point);
            //          }
            //      }
            //
            //      //weapon input PC
            //      if (Input.GetMouseButton(0))
            //      {
            //          m_ammo = m_gunController.m_equippedGun.m_remainingInMag;
            //
            //          m_gunController.OnTriggerHold ();
            //      }
            //
            //      if (Input.GetMouseButtonUp(0))
            //      {
            //          m_ammo = m_gunController.m_equippedGun.m_remainingInMag;
            //          //m_gunController.OnTriggerRelease ();
            //      }
            //
            //      if (Input.GetKeyDown (KeyCode.R)) 
            //{
            //	m_gunController.Reload ();
            //}
    }

    public void MoveStick()
	{
//		if (m_joystick != null)
//		{
//			Debug.Log ("not null");

			if (m_joystick.inputDirection != Vector3.zero) 
			{
				//move calculation
				Vector3 moveInp = new Vector3 (m_joystick.GetPosition ().x * m_moveSpeed, 
					                 0, m_joystick.GetPosition ().z * m_moveSpeed);

				//Debug.Log ("move inp vect" + moveInp);
				Vector3 moveVel = moveInp.normalized * m_moveSpeed;
				//Debug.Log ("move vel" + moveVel);

				m_controller.Move (moveVel);

                m_newPos.x = this.transform.position.x;
                m_newPos.z = this.transform.position.z - 5;
                m_camera.transform.position = m_newPos;
            }

            //else
            //{
            //    m_controller.Move(Vector3.zero);
            //}
		//}
	}

	public void LookStick()
	{
		if (m_lookStick.inputDirection != Vector3.zero) 
		{
			Vector3 lookInp = new Vector3 (m_lookStick.GetPosition ().x, 
			0, m_lookStick.GetPosition ().z);
    
			Vector3 lookVel = lookInp.normalized * m_moveSpeed + transform.position;

			m_controller.LookAt (lookVel);
			m_gunController.OnTriggerHold ();
        } 

		else 
		{
			m_gunController.OnTriggerRelease ();
		}
		//m_ammo = m_gunController.m_equippedGun.m_remainingInMag;
	}

	public override void Die()
	{
        //AudioManager.m_aInstance.PlaySound ("Player Death", transform.position);
        Time.timeScale = 1;
        m_listener.transform.parent = null;
		base.Die ();
        for (int i = 0; i < PoolManager.instance.m_spawnedObjects.Count; i++)
        {
            //deactive spawned enemies
            PoolManager.instance.m_spawnedObjects[i].SetActive(false);
        }
        FindObjectOfType<Spawner>().gameObject.SetActive(false);
        GameObject.Destroy(gameObject);
    }
}
