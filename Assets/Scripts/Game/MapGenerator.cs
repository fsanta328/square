﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour 
{
	public Map[] m_maps;
	public int m_mapIndex;

	public Transform m_tilePrefab;
	public Transform m_obstaclePrefab;

	public Transform m_mapFloor;

	public Transform m_navMeshFloor;
	public Transform m_navMeshMaskPrefab;
	public Vector2 m_maxMapSize;

	[Range(0,1)]
	public float m_outlinePercent;

	public float m_tileSize;

	List<Coord> m_allTileCoords;
	Queue<Coord> m_shuffledTileCoords;
	Queue<Coord> m_shuffledOpenTileCoords;
	Transform[,] m_tileMap;

	Map m_currentMap;
	public GameObject mapHold;

	void Awake () 
	{
		mapHold = this.gameObject;
		if (FindObjectOfType<Spawner> () == null)
			return;
		FindObjectOfType<Spawner> ().OnNewWave += OnNewWave;
		//GenerateMap ();
	}

	void OnNewWave (int a_waveNumber)
	{
		m_mapIndex = a_waveNumber - 1;
		GenerateMap ();
	}

	public void GenerateMap()
	{
		m_currentMap = m_maps [m_mapIndex];
		m_tileMap = new Transform[m_currentMap.m_mapSize.x, m_currentMap.m_mapSize.y];
		System.Random prng = new System.Random (m_currentMap.m_seed);

		// Generating coords
		m_allTileCoords = new List<Coord> ();
		for (int x = 0; x < m_currentMap.m_mapSize.x; x++) 
		{
			for (int y = 0; y < m_currentMap.m_mapSize.y; y++) 
			{
				m_allTileCoords.Add (new Coord(x,y));
			}
		}
		m_shuffledTileCoords = new Queue<Coord> (Utility.ShuffleArray (m_allTileCoords.ToArray (), m_currentMap.m_seed));

		//create map holder object
		string holderName = "Generated Map";
		if (transform.FindChild (holderName))
		{
			DestroyImmediate (transform.FindChild (holderName).gameObject);
		}

		Transform mapHolder = new GameObject (holderName).transform;
		mapHolder.parent = transform;

		//spawning tiles
		for (int x = 0; x < m_currentMap.m_mapSize.x; x++) 
		{
			for (int y = 0; y < m_currentMap.m_mapSize.y; y++) 
			{
				Vector3 tilePosition = CoordToPosition (x, y);
				Transform newTile = Instantiate (m_tilePrefab, tilePosition, Quaternion.Euler (Vector3.right * 90)) as Transform;
				newTile.localScale = Vector3.one * (1 - m_outlinePercent) * m_tileSize;
				newTile.parent = mapHolder;
				m_tileMap [x, y] = newTile;
			}
		}

		bool[,] obstacleMap = new bool[(int)m_currentMap.m_mapSize.x, (int)m_currentMap.m_mapSize.y];

		int obstacleCount = (int)(m_currentMap.m_mapSize.x * m_currentMap.m_mapSize.y * m_currentMap.m_obstaclePercent);
		int a_currentObstacleCount = 0;
		List<Coord> allOpenCoord = new List<Coord> (m_allTileCoords);

		//spawning obstacles
		for (int i = 0; i < obstacleCount; i++) 
		{
			Coord randomCoord = GetRandomCoord ();
			obstacleMap [randomCoord.x, randomCoord.y] = true;
			a_currentObstacleCount++;

			if (randomCoord != m_currentMap.m_mapCentre && MapIsFullyAccesible (obstacleMap, a_currentObstacleCount))
			{
				float obstacleHeight = Mathf.Lerp (m_currentMap.m_minObstacleHeight, m_currentMap.m_maxObstacleHeight, (float)prng.NextDouble ());
				Vector3 obstaclePosition = CoordToPosition (randomCoord.x, randomCoord.y);

				Transform newObstacle = Instantiate (m_obstaclePrefab, obstaclePosition + Vector3.up * obstacleHeight / 2, Quaternion.identity) as Transform; 
				newObstacle.parent = mapHolder;
				newObstacle.localScale = new Vector3((1 - m_outlinePercent) * m_tileSize, obstacleHeight, (1 - m_outlinePercent) * m_tileSize);

				Renderer obstacleRenderer = newObstacle.GetComponent<Renderer> ();
				Material obstacleMaterial = new Material (obstacleRenderer.sharedMaterial);

				float colorPercent = randomCoord.y / (float)m_currentMap.m_mapSize.y;
				obstacleMaterial.color = Color.Lerp (m_currentMap.m_foregroundColor, m_currentMap.m_backgroundColor, colorPercent);
				obstacleRenderer.sharedMaterial = obstacleMaterial;

				allOpenCoord.Remove (randomCoord);
			}

			else
			{
				obstacleMap [randomCoord.x, randomCoord.y] = false;
				a_currentObstacleCount--;
			}
		}

		m_shuffledOpenTileCoords = new Queue<Coord> (Utility.ShuffleArray (allOpenCoord.ToArray (), m_currentMap.m_seed));

		//creating navmesh mask
		Transform maskLeft = Instantiate (m_navMeshMaskPrefab, Vector3.left * (m_currentMap.m_mapSize.x + m_maxMapSize.x) / 4f * m_tileSize, Quaternion.identity) as Transform;
		maskLeft.parent = mapHolder;
		maskLeft.localScale = new Vector3 ((m_maxMapSize.x - m_currentMap.m_mapSize.x) / 2f, 1, m_currentMap.m_mapSize.y) * m_tileSize;

		Transform maskRight = Instantiate (m_navMeshMaskPrefab, Vector3.right * (m_currentMap.m_mapSize.x + m_maxMapSize.x) / 4f * m_tileSize, Quaternion.identity) as Transform;
		maskRight.parent = mapHolder;
		maskRight.localScale = new Vector3 ((m_maxMapSize.x - m_currentMap.m_mapSize.x) / 2f, 1, m_currentMap.m_mapSize.y) * m_tileSize;

		Transform maskTop = Instantiate (m_navMeshMaskPrefab, Vector3.forward * (m_currentMap.m_mapSize.y + m_maxMapSize.y) / 4f * m_tileSize, Quaternion.identity) as Transform;
		maskTop.parent = mapHolder;
		maskTop.localScale = new Vector3 (m_maxMapSize.x, 1, (m_maxMapSize.y - m_currentMap.m_mapSize.y) / 2f) * m_tileSize;

		Transform maskBottom = Instantiate (m_navMeshMaskPrefab, Vector3.back * (m_currentMap.m_mapSize.y + m_maxMapSize.y) / 4f * m_tileSize, Quaternion.identity) as Transform;
		maskBottom.parent = mapHolder;
		maskBottom.localScale = new Vector3 (m_maxMapSize.x, 1, (m_maxMapSize.y - m_currentMap.m_mapSize.y) / 2f) * m_tileSize;

		m_navMeshFloor.localScale = new Vector3 (m_maxMapSize.x, m_maxMapSize.y) * m_tileSize;
		m_mapFloor.localScale = new Vector3 (m_currentMap.m_mapSize.x * m_tileSize, m_currentMap.m_mapSize.y * m_tileSize);
	}

	//flood fill algorithm
	bool MapIsFullyAccesible (bool[,] a_obstacleMap, int a_currentObstacleCount)
	{
		bool[,] mapFlags = new bool[a_obstacleMap.GetLength (0), a_obstacleMap.GetLength (1)];
		Queue<Coord> queue = new Queue<Coord> ();
		queue.Enqueue (m_currentMap.m_mapCentre);
		mapFlags [m_currentMap.m_mapCentre.x,m_currentMap.m_mapCentre.y] = true;

		int accessibleTileCount = 1;

		while (queue.Count > 0)
		{
			Coord tile = queue.Dequeue ();

			for (int x = -1; x <= 1; x++) 
			{
				for (int y = -1; y <= 1; y++)
				{
					int neighbourX = tile.x + x;
					int neighbourY = tile.y + y;
					if (x == 0 || y == 0) 
					{
						if (neighbourX >= 0 && neighbourX < a_obstacleMap.GetLength (0)
						    && neighbourY >= 0 && neighbourY < a_obstacleMap.GetLength (1)) 
						{
							if (!mapFlags [neighbourX, neighbourY] && !a_obstacleMap [neighbourX, neighbourY]) 
							{
								mapFlags [neighbourX, neighbourY] = true;
								queue.Enqueue (new Coord(neighbourX, neighbourY));
								accessibleTileCount++;
							}
						}
					}
				}
			}
		}

		int targetAccesibleTileCount = (int)(m_currentMap.m_mapSize.x * m_currentMap.m_mapSize.y - a_currentObstacleCount);
		return targetAccesibleTileCount == accessibleTileCount;
	}

	Vector3 CoordToPosition(int a_x, int a_y)
	{
		return new Vector3 (-m_currentMap.m_mapSize.x / 2f + 0.5f + a_x, 0, -m_currentMap.m_mapSize.y / 2f + 0.5f + a_y) * m_tileSize;
	}

	public Transform GetTileFromPosition (Vector3 a_position)
	{
		int x = Mathf.RoundToInt(a_position.x / m_tileSize + (m_currentMap.m_mapSize.x - 1) / 2f);
		int y = Mathf.RoundToInt(a_position.z / m_tileSize + (m_currentMap.m_mapSize.y - 1) / 2f);
		x = Mathf.Clamp (x, 0, m_tileMap.GetLength (0) -1);
		y = Mathf.Clamp (y, 0, m_tileMap.GetLength (1) -1);

		return m_tileMap [x, y];
	}

	public Coord GetRandomCoord()
	{
		Coord randomCoord = m_shuffledTileCoords.Dequeue ();
		m_shuffledTileCoords.Enqueue (randomCoord);
		return randomCoord;
	}

	public Transform GetRandomOpenTile()
	{
		Coord randomCoord = m_shuffledOpenTileCoords.Dequeue ();
		m_shuffledOpenTileCoords.Enqueue (randomCoord);
		return m_tileMap [randomCoord.x, randomCoord.y];
	}

//	IEnumerator AnimateMenuMap()
//	{
//		Debug.Log ("starting map anim");
//		float delayTime = 3f;
//		float speed = .1f;
//		float animationPercent = 0;
//		int dir = 1;
//		float endDelayTime = Time.time + 1 / speed + delayTime;
//
//		while (animationPercent >= 0) 
//		{
//			animationPercent += Time.deltaTime * speed * dir;
//
//			if (animationPercent >= 1)
//			{
//				animationPercent = 1;
//				if (Time.time > endDelayTime) 
//				{
//					dir = -1;
//				}
//			}
//
//			m_outlinePercent =  Mathf.Lerp (0.1f, 1f, animationPercent);
//			GenerateMap ();
//			yield return null;
//		}
	//}

	//public void Sink()
	//{
		//Coord tileCoord = m_shuffledTileCoords.Dequeue ();
		//m_currentMap = m_maps [m_mapIndex];
		//m_tileMap = new Transform[m_currentMap.m_mapSize.x, m_currentMap.m_mapSize.y];
		//for (int x = 0; x < m_currentMap.m_mapSize.x; x++) 
		//{
			//for (int y = 0; y < m_currentMap.m_mapSize.y; y++) 
			//{
				//Debug.Log("moving up");
				//m_tileMap [x, y].gameObject.transform.position = Vector3.up * Time.deltaTime * 3f;
			//}
		//}

	//	GameObject mapHolder = GameObject.Find ("Generated Map");
	//	foreach (Transform mapPiece in mapHolder.transform) 
	//	{
	//		float sinkSpeed = Random.Range (1f, 4f);
	//		mapPiece.transform.position = mapPiece.transform.position + (Vector3.down * Time.deltaTime * sinkSpeed);
	//	}
	//}


	//public void SinkMap()
	//{
	//	Transform start = this.transform;
	//	Vector3 end = new Vector3 (start.position.x, start.position.y - 5, start.position.z);
	//	mapHold.transform.position = Vector3.Lerp (start.position, end, 3);
	//}

	//public void RaiseMap()
	//{
	//	Transform start = this.transform;
	//	Vector3 end = new Vector3 (start.position.x, start.position.y + 5, start.position.z);
	//	mapHold.transform.position = Vector3.Lerp (start.position, end, 3);
	//}

	//public IEnumerator SinkM()
	//{
	//	float delayTime = 2f;
	//	float speed = 1f;
	//	float animationPercent = 0;
	//	int dir = 1;
	//	float endDelayTime = Time.time + 1 / speed + delayTime;
	//	GameObject map = GameObject.Find ("Map");

	//	while (animationPercent >= 0) 
	//	{
	//		animationPercent += Time.deltaTime * speed * dir;

	//		if (animationPercent >= 1)
	//		{
	//			animationPercent = 1;
	//			if (Time.time > endDelayTime) 
	//			{
	//				dir = -1;
	//			}
	//		}

	//		map.gameObject.transform.position = Vector2.up * Mathf.Lerp (0, 195, animationPercent);
	//		yield return null;
	//	}
	//}

	//void Update()
	//{
		//if (Input.GetKeyDown (KeyCode.S)) 
		//{
		//	SinkMap ();
		//}
//		if (Menu.inGame)
//			return;
//		else
//		{
//			//code to rotate map
//			if (SceneManager.GetActiveScene ().buildIndex == 0) 
//			{
//				//Debug.Log ("Menu");
//				transform.parent.Rotate (Vector3.up * Time.deltaTime * 15);
//				if (transform.rotation.eulerAngles.y >= 70 && transform.rotation.eulerAngles.y <= 75 && m_hasRotated == true)
//				{
//					m_hasRotated = false;
//					m_mapIndex = m_mapIndex+1;
//					if (m_mapIndex >5)
//					{
//						m_mapIndex = 0;
//						GenerateMap();
//					}
//
//					else
//						GenerateMap ();
//				}
//
//				if (transform.rotation.eulerAngles.y <= 50)
//					m_hasRotated = true;
//
//				if (Input.GetKey (KeyCode.C)) 
//				{
//					Sink ();
//				}
//			}
		//}
	//}

    public IEnumerator MinimizeMap ()
    {
        yield return new WaitForSeconds(1.5f);

        int a_time = 1;
        float speed = 1 / a_time;
        float percent = 0;
        float m_startSize = m_outlinePercent;
        //Debug.Log(percent);

        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            m_outlinePercent = Mathf.Lerp(m_startSize, 1.0f, percent);
            //Debug.Log(percent);
            GenerateMap();
            yield return null;
        }
        StopCoroutine("MinimizeMap");
    }

	[System.Serializable]
	public struct Coord
	{
		public int x;
		public int y;

		public Coord(int a_x, int a_y)
		{
			x = a_x;
			y = a_y;
		}

		public static bool operator == (Coord c1, Coord c2)
		{
			return c1.x == c2.x && c1.y == c2.y;
		}

		public static bool operator != (Coord c1, Coord c2)
		{
			return !(c1 == c2);
		}
	}

	[System.Serializable]
	public class Map
	{
		public Coord m_mapSize;

		[Range(0,1)]
		public float m_obstaclePercent;
		public int m_seed;
		public float m_minObstacleHeight;
		public float m_maxObstacleHeight;
		public Color m_foregroundColor;
		public Color m_backgroundColor;

		public Coord m_mapCentre{get {return new Coord (m_mapSize.x / 2, m_mapSize.y / 2);}}
	}
}
