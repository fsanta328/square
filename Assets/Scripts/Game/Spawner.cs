﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;

public class Spawner : MonoBehaviour 
{
	public bool devMode;

	public Wave[] m_waves;
	public Enemy m_enemy;

	LivingEntity m_playerEntity;
	Transform m_playerTransform;

	Wave m_currentWave;
	int m_currentWaveNum;

	int m_enemiesRemainingToSpawn;
	int m_enemiesAlive;
	float m_nextSpawnTime;

	MapGenerator m_map;

	float m_stationaryCheck = 2;
	float m_campThresholdDistance = 1.5f;
	float m_nextStationaryCheck;
	Vector3 campPosOld;
	bool m_isStationary;

	bool m_isDisabled;

	public event System.Action<int> OnNewWave;
    public static float timer = 0;
    //float waveTime;

    //NEW
    bool m_first = true;
    int m_numOfEnemiesSpawned = 0;
    GameObject[] m_enemyArray = new GameObject[11];

    void Start()
	{
		m_playerEntity = FindObjectOfType<Player> ();
		m_playerTransform = m_playerEntity.transform;

		m_nextStationaryCheck = m_stationaryCheck + Time.time;
		campPosOld = m_playerTransform.position;
		m_playerEntity.OnDeath += OnPlayerDeath;

		m_map = FindObjectOfType<MapGenerator> ();

        for (int i = 0; i < m_enemyArray.Length; i++)
        {
            GameObject a_enemy = Instantiate(m_enemy.gameObject);
            a_enemy.SetActive(false);
            a_enemy.GetComponent<Enemy>().OnDeath += OnEnemyDeath;
            m_enemyArray[i] = a_enemy;
        }

        PoolManager.instance.CreateList(m_enemyArray, m_enemyArray.Length);
        //PoolManager.instance.FillPool(m_enemyArray, 9);
        NextWave();
	}

	void Update()
	{
        //timer += Time.deltaTime;
        if (!m_isDisabled)
		{
			if (Time.time > m_nextStationaryCheck) 
			{
				m_nextStationaryCheck = Time.time + m_stationaryCheck;

				m_isStationary = (Vector3.Distance (m_playerTransform.position, campPosOld) < m_campThresholdDistance);

				campPosOld = m_playerTransform.position;
			}

			if ((m_enemiesRemainingToSpawn > 0 || m_currentWave.m_infinite) && Time.time > m_nextSpawnTime && m_numOfEnemiesSpawned < 10) 
			{
				m_nextSpawnTime = Time.time + m_currentWave.m_timeBetweenSpawns;

				StartCoroutine ("SpawnEnemy");
			}
		}

		//if (devMode) 
		//{
		//	if (Input.GetKey (KeyCode.Return))
		//	{
		//		StopCoroutine ("SpawnEnemy");
		//		foreach (Enemy enemy in FindObjectsOfType<Enemy>()) 
		//		{
		//			GameObject.Destroy (enemy.gameObject);
		//		}
		//		NextWave ();
		//	}
		//}
	}

	IEnumerator SpawnEnemy()
	{
        if (m_first)
        {
            yield return new WaitForSeconds(1.5f);
        }

        //limit the total amount of enemies on field
        //if (m_numOfEnemiesSpawned < 4)
        //{
        m_enemiesRemainingToSpawn--;
        m_first = false;
            float spawnDelay = 1;
            float tileFlashSpeed = 4;

            Transform spawnTile = m_map.GetRandomOpenTile();

            if (m_isStationary)
            {
                spawnTile = m_map.GetTileFromPosition(m_playerTransform.position);
            }
            Material tileMat = spawnTile.GetComponent<Renderer>().material;
            Color initialColor = new Color32(0xA3, 0xA0, 0xA0, 0xFF);
            Color flashColor1 = Color.white;
            Color flashColor2 = Color.red;
            float spawnTimer = 0;

            while (spawnTimer < spawnDelay)
            {
                tileMat.color = Color.Lerp(flashColor1, flashColor2, Mathf.PingPong(spawnTimer * tileFlashSpeed, 1));

                spawnTimer += Time.deltaTime;
                yield return null;
            }

            Enemy a_enemyNew = PoolManager.instance.AddToActive(/*m_enemyArray[0].gameObject*/).GetComponent<Enemy>();
            ReuseEnemy(a_enemyNew, spawnTile.position);

            //Enemy a_enemyNew = PoolManager.instance.ReuseObject(m_enemyArray[0].gameObject/*m_enemy.gameObject*/).GetComponent<Enemy>();
            //ReuseEnemy(a_enemyNew, spawnTile.position);

            //Enemy spawnedEnemy = Instantiate(m_enemy, spawnTile.position + Vector3.up, Quaternion.identity) as Enemy;

            tileMat.color = initialColor;

            //spawnedEnemy.OnDeath += OnEnemyDeath;
            //spawnedEnemy.SetCharacteristics(m_currentWave.m_moveSpeed, m_currentWave.m_hitsToKillPlayer, m_currentWave.m_enemyHealth, m_currentWave.m_skinColor);

            m_numOfEnemiesSpawned++;
            //Debug.Log("Enemies remaining to spawn " + m_enemiesRemainingToSpawn);
            
        //}
	}

    void ReuseEnemy(Enemy a_enemy, Vector3 a_pos)
    {
        a_enemy.transform.position = (a_pos + Vector3.up);
        a_enemy.SetCharacteristics(m_currentWave.m_moveSpeed, m_currentWave.m_hitsToKillPlayer, m_currentWave.m_enemyHealth, m_currentWave.m_skinColor);
        a_enemy.gameObject.SetActive(true);
    }

	void OnPlayerDeath()
	{
		m_isDisabled = true;
	}

	void OnEnemyDeath()
	{
		m_enemiesAlive--;
        m_numOfEnemiesSpawned--;
        //Debug.Log("Enemies left " + m_enemiesAlive);

        //return to useable list
        

		if (m_enemiesAlive == 0) 
		{
            //add delay screen, rewards, kills, accuracy?
            //waveTime = timer;
            //print(waveTime);
            //timer = 0;
            m_first = true;
            m_numOfEnemiesSpawned = 0;
            NextWave ();
		}
	}

	void ResetPlayPosition()
	{
		m_playerTransform.position = m_map.GetTileFromPosition(Vector3.zero).position + Vector3.up * 1.2f;
	}

	public void NextWave()
	{ 
		if (m_currentWaveNum > 0) 
		{
			AudioManager.m_aInstance.PlaySound2D ("Level Complete");
		}

		m_currentWaveNum++;
		if (m_currentWaveNum - 1 < m_waves.Length)
		{
			m_currentWave = m_waves [m_currentWaveNum - 1];

			m_enemiesRemainingToSpawn = m_currentWave.m_enemyCount;
			m_enemiesAlive = m_enemiesRemainingToSpawn;
            //Debug.Log("enemies to spawn = "+ m_enemiesAlive);

			if (OnNewWave != null) 
			{
				OnNewWave (m_currentWaveNum);
			}
			ResetPlayPosition ();
		}
	}

	//public int lv = 0;
	//public void Skip()
	//{
	//	if (lv < 5)
    //       {
	//		StopCoroutine ("SpawnEnemy");
	//		foreach (Enemy enemy in FindObjectsOfType<Enemy>())
    //           {
	//			GameObject.Destroy (enemy.gameObject);
	//		}
	//		NextWave ();
	//		lv++;
	//	}
	//}

	[System.Serializable]
	public class Wave
	{
		public bool m_infinite;
		public int m_enemyCount;
		public float m_timeBetweenSpawns;

		public float m_moveSpeed;
		public int m_hitsToKillPlayer;
		public float m_enemyHealth;
		public Color m_skinColor;
	}
}
