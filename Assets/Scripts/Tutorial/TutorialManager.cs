﻿using UnityEngine;
using System.Collections;

public class TutorialManager : MonoBehaviour
{
    int m_taskNumber = 0;
    bool m_taskComplete = true;
    public Spawner m_spawner;
    public GameObject m_leftStick;
    public GameObject m_rightStick;
    public GameObject m_player;
    public GameObject m_weaponHolder;
    public Transform m_startPosition;
    public TutorialUI m_tutorialUI;

	// Use this for initialization
	void Start ()
    {
        m_startPosition = m_player.transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    public void Task()
    {
        if (m_taskComplete)
        {
            switch (m_taskNumber)
            {
                //get player to move around
                case (0):
                    break;

                //use right stick
                case (1):
                    break;

                //learn ammo and reload feature
                case (2):
                    break;

                //kill enemy
                case (3):
                    m_spawner.NextWave();
                    m_taskNumber = 4;
                    break;

                case (4):
                    break;
            }
        }
    }

    public void PopUpInfo()
    {
        m_tutorialUI.PopUpText(m_taskNumber);
    }
}
