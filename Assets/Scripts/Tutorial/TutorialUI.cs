﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialUI : MonoBehaviour
{
    public Image m_fadePlane;
    public GameObject m_gameOverUI;

    public RectTransform m_healthBar;
    public GameObject m_gameUIHolder;
    public GameObject m_PauseMenuHolder;
    public Text m_ammoCount;

    Spawner m_spawner;
    Player m_player;

    //NEW
    public Text m_lobbyMessageText;
    float healthPercent = 0;
    public Slider[] m_volumeSliders;
    public static bool m_paused = false;

    void Awake()
    {
        m_spawner = FindObjectOfType<Spawner>();
        //m_spawner.OnNewWave += OnNewWave;
    }

    void Start()
    {
        m_player = FindObjectOfType<Player>();
        m_player.OnDeath += OnGameOver;        

        //if (AudioManager.m_aInstance != null)
        //{
        //    m_volumeSliders[0].value = AudioManager.m_aInstance.m_masterVolumePercent;
        //    m_volumeSliders[1].value = AudioManager.m_aInstance.m_musicVolumePercent;
        //    m_volumeSliders[2].value = AudioManager.m_aInstance.m_sfxVolumerPercent;
        //}
    }

    void Update()
    {
        if (m_player != null)
        {
            healthPercent = m_player.m_health / m_player.m_startHealth;
            m_healthBar.localScale = new Vector3(healthPercent, 1, 1);
            //m_ammoCount.text = m_player.m_ammo.ToString();
        }
    }

    public void PopUpText(int a_int)
    {
        switch (a_int)
        {
            case 0:
                break;

            case 1:
                break;

            case 2:
                break;

            default:
                break;
        }
    }

    void OnGameOver()
    {
        StartCoroutine(Fade(Color.clear, new Color(0, 0, 0, .95f), 1));
        m_healthBar.transform.parent.gameObject.SetActive(false);
        m_gameOverUI.SetActive(true);
    }

    IEnumerator Fade(Color a_from, Color a_to, float a_time)
    {
        float speed = 1 / a_time;
        float percent = 0;

        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            m_fadePlane.color = Color.Lerp(a_from, a_to, percent);
            yield return null;
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ReturnToMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void PauseMenu()
    {
        m_paused = true;
        m_gameUIHolder.SetActive(false);
        m_PauseMenuHolder.SetActive(true);
        Time.timeScale = 0;
    }

    public void Resume()
    {
        m_paused = false;
        Time.timeScale = 1;
        m_gameUIHolder.SetActive(true);
        m_PauseMenuHolder.SetActive(false);
    }

    public void SetMasterVolume(float a_volume)
    {
        AudioManager.m_aInstance.SetVolume(a_volume, AudioManager.AudioChannel.Master);
    }

    public void SetMusicVolume(float a_volume)
    {
        AudioManager.m_aInstance.SetVolume(a_volume, AudioManager.AudioChannel.Music);
    }

    public void SetSfxVolume(float a_volume)
    {
        AudioManager.m_aInstance.SetVolume(a_volume, AudioManager.AudioChannel.Sfx);
    }

}
