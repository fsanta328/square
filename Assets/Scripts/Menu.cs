﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
	public GameObject m_mainMenuHolder;
	public GameObject m_optionsMenuHolder;
	public Slider[] m_volumeSliders;
	public Gradient m_gradient;
	public float m_duration;
	public Image[] m_mainMenuButtons;

	//NEW
	public Text m_lobbyMessageText;
    public MapGenerator m_menuMap;
    public GameObject m_floor;
    public Animator m_optionAnimator;
    public Animator m_infoAnimator;
    public GameObject m_gpgInfo;
    public GameObject m_popUp;
    public static int m_signedIn = 0;

	void Awake()
	{
        m_lobbyMessageText = m_lobbyMessageText.GetComponent<Text>();
    }

	void Start () 
	{
        int loadIn = PlayerPrefs.GetInt("FirstLoad", 0);

        //player uses gpg account
        if (loadIn == 1 && (PlayerPrefs.GetInt("FirstLoad") == 1))
        {
            GPGSS.Instance.TrySilentSignIn();
            m_mainMenuHolder.SetActive(true);
            //m_signedIn = loadIn;
        }

        //no gpg account
        else if (loadIn == 1 && (PlayerPrefs.GetInt("FirstLoad") == 0))
        {
            //perform checks for any area of the game that calls gpg and avoid
            //m_signedIn = loadIn;
            m_mainMenuHolder.SetActive(true);
        }

        //game has been loaded for the first time
        if (loadIn == 0)
        {
            //pop up sign in box
            PopUp();
            //save selection for subsequent plays
            //set first load to 1
            PlayerPrefs.SetInt("FirstLoad", 1);
        }

        //loadIn = PlayerPrefs.GetInt("SignInPref" , 0);

        ////player uses gpg account
        //if (loadIn == 1 && (PlayerPrefs.GetInt("FirstLoad") == 1))
        //{
        //    GPGSS.Instance.TrySilentSignIn();
        //    m_mainMenuHolder.SetActive(true);
        //    //m_signedIn = loadIn;
        //}

        ////no gpg account
        //else if (loadIn == 0 && (PlayerPrefs.GetInt("FirstLoad") == 1))
        //{
        //    //perform checks for any area of the game that calls gpg and avoid
        //    //m_signedIn = loadIn;
        //    m_mainMenuHolder.SetActive(true);
        //}

        if (AudioManager.m_aInstance != null)
        {
            m_volumeSliders[0].value = AudioManager.m_aInstance.m_masterVolumePercent;
            m_volumeSliders[1].value = AudioManager.m_aInstance.m_musicVolumePercent;
            m_volumeSliders[2].value = AudioManager.m_aInstance.m_sfxVolumerPercent;
        }
        PlayerPrefs.Save();
        StartCoroutine("MoveFloorFiller");
    }

	void Update()
	{
        if (m_mainMenuHolder.activeSelf == true)
        {
            GradientColor();
        }
    }

    void PopUp()
    {
        m_popUp.SetActive(true);
    }

    //user wants to use GPG account
    public void ConfirmPopUp()
    {
        PlayerPrefs.SetInt("SignInPref", 1);
        PlayerPrefs.Save();
        SignIn();
        m_popUp.SetActive(false);
        m_mainMenuHolder.SetActive(true);
    }

    //not use GPG account
    public void DenyPopUp()
    {
        PlayerPrefs.SetInt("SignInPref", 0);
        PlayerPrefs.Save();
        m_popUp.SetActive(false);
        m_mainMenuHolder.SetActive(true);
    }

    void GradientColor()
	{
		float t = Mathf.Repeat(Time.time, m_duration) / m_duration;

        for (int i = 0; i < m_mainMenuButtons.Length; i++)
        {
            m_mainMenuButtons[i].color = m_gradient.Evaluate(t);
        }
	}

	public void Play()
	{
        m_signedIn = PlayerPrefs.GetInt("SignInPref");
        StopCoroutine("MoveFloorFiller");
        SceneManager.LoadScene ("Game");
	}

	public void Quit()
	{
		Application.Quit ();
	}

	public void OptionsMenu()
	{
		m_mainMenuHolder.SetActive (false);
		m_optionsMenuHolder.SetActive (true);
        m_optionAnimator.SetBool("IsHidden", false);
    }

	public void MainMenu()
	{
        m_optionAnimator.SetBool("IsHidden", true);
        m_gpgInfo.SetActive(false);
        m_mainMenuHolder.SetActive (true);
    }

    public void HideOptions()
    {
        m_optionAnimator.SetBool("IsHidden", true);
        m_gpgInfo.SetActive(true);
        m_infoAnimator.SetBool("IsHidden", true);
    }

    public void ReturnOptions()
    {
        m_optionAnimator.SetBool("IsHidden", false);
        m_infoAnimator.SetBool("IsHidden", false);
    }

    IEnumerator MoveFloorFiller()
    {
        yield return new WaitForSeconds(5);
        float delayTime = 2f;
        float speed = .4f;
        float animationPercent = 0;
        int dir = 1;
        float endDelayTime = Time.time + 1 / speed + delayTime;

        while (animationPercent >= 0)
        {
            animationPercent += Time.deltaTime * speed * dir;
            if (animationPercent >= 1)
            {
                animationPercent = 1;
                if (Time.time > endDelayTime)
                {
                    m_menuMap.m_mapIndex++;
                    if (m_menuMap.m_mapIndex >= m_menuMap.m_maps.Length)
                    {
                        m_menuMap.m_mapIndex = 0;
                    }
                    m_menuMap.GenerateMap();
                    dir = -1;
                }
            }

            m_floor.transform.position = Vector3.up * Mathf.Lerp(-1, 6, animationPercent);
            yield return null;
        }
        StartCoroutine("MoveFloorFiller");
    }

    public void SignIn ()
	{
        m_lobbyMessageText.text = GPGSS.Instance.SignIn ();
        PlayerPrefs.SetInt("SignInPref", 1);
        PlayerPrefs.Save(); 
    }

    public void CheckAccount ()
    {
        HideOptions();
        if (!GPGSS.Instance.IsAuthenticated())
        {
            m_lobbyMessageText.text = "You are not signed in.";
        }

        else
            m_lobbyMessageText.text = "You are already signed in.";
    }

	public void SignOut ()
	{
		if (GPGSS.Instance.IsAuthenticated ())
		{
			GPGSS.Instance.SignOut ();
			m_lobbyMessageText.text = "user signed out";
            PlayerPrefs.SetInt("SignInPref", 0);
        }

		else
			m_lobbyMessageText.text = "account not signed in";

        PlayerPrefs.Save();
    }

    public void ShowAchievements()
    {
        if (!GPGSS.Instance.IsAuthenticated())
        {
            SignIn();
        }

        else
            Social.ShowAchievementsUI();
    }

	public void ShowLeaderboards()
	{
		if (!GPGSS.Instance.IsAuthenticated ())
		{
			SignIn ();
		}

		else
			Social.ShowLeaderboardUI();
	}

	public void SetMasterVolume(float a_volume)
	{
		AudioManager.m_aInstance.SetVolume (a_volume, AudioManager.AudioChannel.Master);
	}

	public void SetMusicVolume(float a_volume)
	{
		AudioManager.m_aInstance.SetVolume (a_volume, AudioManager.AudioChannel.Music);
	}

	public void SetSfxVolume(float a_volume)
	{
		AudioManager.m_aInstance.SetVolume (a_volume, AudioManager.AudioChannel.Sfx);
	}
}
