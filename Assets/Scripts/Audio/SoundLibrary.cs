﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundLibrary : MonoBehaviour {

	public SoundGroup[] m_SoundGroups;
	Dictionary<string, AudioClip[]> m_groupDictionary = new Dictionary<string, AudioClip[]>();

	void Awake()
	{
		foreach (SoundGroup soundGroup in m_SoundGroups) 
		{
			m_groupDictionary.Add (soundGroup.m_groupID, soundGroup.m_group);
		}
	}

	public AudioClip GetClipFromName(string a_name)
	{
		if (m_groupDictionary.ContainsKey (a_name)) 
		{
			AudioClip[] sounds = m_groupDictionary [a_name];
			return sounds [Random.Range (0, sounds.Length)];
		}
		return null;
	}

	[System.Serializable]
	public class SoundGroup
	{
		public string m_groupID;
		public AudioClip[] m_group;
	}
}
