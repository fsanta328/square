﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour 
{
	public AudioClip m_mainTheme;
	public AudioClip m_menuTheme;

	int m_sceneIndex;

	void Start ()
	{
        //does not load on the first build scene
        //OnLevelFinishedLoading(0);
        //print("m1");
	}

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        int newSceneIndex = SceneManager.GetActiveScene().buildIndex;
        m_sceneIndex = newSceneIndex;
        Invoke("PlayMusic", .2f);
    }

 //   void OnLevelWasLoaded (int a_sceneIndex)
	//{
 //       int newSceneIndex = SceneManager.GetActiveScene().buildIndex;
 //       //print("m2");
 //       //Debug.Log("Loaded");
 //       //      if ((newSceneIndex == 1 /*!= m_sceneIndex*/) || (newSceneIndex == 0)) 
 //       //{
 //       //          print("loaded");
 //       //          m_sceneIndex = newSceneIndex;
 //       //	Invoke ("PlayMusic", .2f);
 //       //}

 //       m_sceneIndex = newSceneIndex;
 //       Invoke("PlayMusic", .2f);
 //   }

	void PlayMusic()
	{
		AudioClip clipToPlay = null;
        if (m_sceneIndex == 0) 
		{
            //print("menu song");
            clipToPlay = m_menuTheme;
		} 

		else if (m_sceneIndex == 1) 
		{
            //print("game song");
            clipToPlay = m_mainTheme;
		}

		if (clipToPlay != null) 
		{
            //print("play song");
            AudioManager.m_aInstance.PlayMusic (clipToPlay, 2);
			//Invoke ("PlayMusic", clipToPlay.length);
		}
	}
}
