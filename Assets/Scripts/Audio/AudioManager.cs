﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour 
{
	public enum AudioChannel
	{
		Master, 
		Sfx,
		Music,
	};

	public float m_masterVolumePercent {get; private set;}
	public float m_sfxVolumerPercent {get; private set;}
	public float m_musicVolumePercent {get; private set;}

	AudioSource m_sfx2DSource;
	AudioSource[] m_musicSources;
	int m_activeMusicSourceIndex;

	public static AudioManager m_aInstance;

	SoundLibrary m_library;

	void Awake()
	{
		if (m_aInstance != null)
		{
			Destroy (gameObject);
		}

		else 
		{
			m_aInstance = this;
			DontDestroyOnLoad (gameObject);
            Application.targetFrameRate = 30;
            m_library = GetComponent<SoundLibrary> ();
			m_musicSources = new AudioSource[2];
			for (int i = 0; i < m_musicSources.Length; i++) 
			{
				GameObject newMusicSource = new GameObject ("Music source " + (i + 1));
				m_musicSources [i] = newMusicSource.AddComponent<AudioSource> ();
				newMusicSource.transform.parent = transform;
			}
			GameObject newSfx2DSource = new GameObject ("2D sfx source");
			m_sfx2DSource = newSfx2DSource.AddComponent<AudioSource> ();
			newSfx2DSource.transform.parent = transform;

			//m_audioListener = FindObjectOfType<AudioListener> ().transform;
			//if (FindObjectOfType<Player> () != null) 
			//{
				//m_playerTransform = FindObjectOfType<Player> ().transform;
			//}
			m_masterVolumePercent = PlayerPrefs.GetFloat ("master vol", 1);
			m_sfxVolumerPercent = PlayerPrefs.GetFloat ("sfx vol", 1);
			m_musicVolumePercent = PlayerPrefs.GetFloat ("music vol", 1);
		}
	}

	public void SetVolume (float a_volumePercent, AudioChannel a_channel)
	{
		switch (a_channel)
		{
			case AudioChannel.Master:
				m_masterVolumePercent = a_volumePercent;
				break;

			case AudioChannel.Sfx:
				m_sfxVolumerPercent = a_volumePercent;
				break;

			case AudioChannel.Music:
				m_musicVolumePercent = a_volumePercent;
				break;

			default:
				break;
		}
		m_musicSources [0].volume = m_musicVolumePercent * m_masterVolumePercent;
		m_musicSources [1].volume = m_musicVolumePercent * m_masterVolumePercent;

		PlayerPrefs.SetFloat ("master vol", m_masterVolumePercent);
		PlayerPrefs.SetFloat ("sfx vol", m_sfxVolumerPercent);
		PlayerPrefs.SetFloat ("music vol", m_musicVolumePercent);
		PlayerPrefs.Save ();
	}

	public void PlaySound2D (string a_soundName)
	{
		m_sfx2DSource.PlayOneShot (m_library.GetClipFromName (a_soundName), m_sfxVolumerPercent * m_masterVolumePercent);
	}

	public void PlayMusic (AudioClip a_clip, float a_fadeDuration)
	{
		if (a_clip != null) 
		{
			m_activeMusicSourceIndex = 1 - m_activeMusicSourceIndex;
			m_musicSources [m_activeMusicSourceIndex].clip = a_clip;
			m_musicSources [m_activeMusicSourceIndex].Play ();
            m_musicSources[m_activeMusicSourceIndex].loop = true;

            StartCoroutine (AnimateMusicCrossFade (a_fadeDuration));
		}
	}

    public void StopMusic ()
    {
        float a_fadeDuration = 2f;
        //m_activeMusicSourceIndex = 1 - m_activeMusicSourceIndex;
        //m_musicSources[m_activeMusicSourceIndex]
        StartCoroutine(AnimateMusicFadeOut(a_fadeDuration));
    }

    IEnumerator AnimateMusicFadeOut (float a_duration)
    {
        float percent = 0;
        while (percent < 1)
        {
            percent += Time.deltaTime * 1 / a_duration;
            m_musicSources[m_activeMusicSourceIndex].volume = Mathf.Lerp(m_musicVolumePercent * m_masterVolumePercent, 0, percent);
            yield return null;
        }
        m_musicSources[m_activeMusicSourceIndex].Stop();
        //Debug.Log("stopped");
    }

	IEnumerator AnimateMusicCrossFade (float a_duration)
	{
		float percent = 0;
		while (percent < 1)
		{
			percent += Time.deltaTime * 1 / a_duration;
			m_musicSources [m_activeMusicSourceIndex].volume = Mathf.Lerp (0, m_musicVolumePercent * m_masterVolumePercent, percent);
			m_musicSources [1 - m_activeMusicSourceIndex].volume = Mathf.Lerp (m_musicVolumePercent * m_masterVolumePercent, 0, percent);
			yield return null;
		}
	}

	public void PlaySound(AudioClip a_clip, Vector3 a_pos)
	{
		AudioSource.PlayClipAtPoint (a_clip, a_pos, m_sfxVolumerPercent * m_masterVolumePercent);
	}

	public void PlaySound(string a_soundName, Vector3 pos)
	{
		PlaySound (m_library.GetClipFromName (a_soundName), pos);
	}
}
