﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour
{
    public Dictionary<int, Queue<GameObject>> poolDictionary = new Dictionary<int, Queue<GameObject>>();
    public List<GameObject> m_spawnableObjects = new List<GameObject>();
    public List<GameObject> m_spawnedObjects = new List<GameObject>();

    static PoolManager _instance;

    public static PoolManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<PoolManager>();
            }
            return _instance;
        }
    }

    public void CreatePool(GameObject prefab, int poolSize)
    {
        int poolKey = prefab.GetInstanceID();

        if (!poolDictionary.ContainsKey(poolKey))
        {
            poolDictionary.Add(poolKey, new Queue<GameObject>());

            for (int i = 0; i < poolSize; i++)
            {
                GameObject newObject = (Instantiate(prefab) as GameObject);
                newObject.SetActive(false);
                poolDictionary[poolKey].Enqueue(newObject);
            }
        }
    }

    public void FillPool(GameObject[] gObject, int poolSize)
    {
        int poolKey = gObject[0].GetInstanceID();

        if (!poolDictionary.ContainsKey(poolKey))
        {
            poolDictionary.Add(poolKey, new Queue<GameObject>());

            for (int i = 0; i < poolSize; i++)
            {
                poolDictionary[poolKey].Enqueue(gObject[i]);
            }
        }
    }

    public void CreateList(GameObject[] prefab, int a_size)
    {
        for (int i = 0; i < a_size; i++)
        {
            m_spawnableObjects.Add(prefab[i]);
        }
    }

    public GameObject ReuseObject(GameObject prefab)
    {
        int poolKey = prefab.GetInstanceID();

        if (poolDictionary.ContainsKey(poolKey))
        {
            GameObject objectToReuse = poolDictionary[poolKey].Dequeue();
            poolDictionary[poolKey].Enqueue(objectToReuse);
            return objectToReuse;
            //objectToReuse.Reuse(position, rotation);
        }
        return null;
    }

    //move object from useable to in use
    public GameObject AddToActive(/*GameObject prefab*/)
    {
        if (m_spawnableObjects[0] != null)
        {
            GameObject objectToSpawn = m_spawnableObjects[0];
            m_spawnableObjects.RemoveAt(0);
            m_spawnedObjects.Add(objectToSpawn);
            return objectToSpawn;
        }
        return null;
    }

    //move from in use to useable
    public void RemoveFromActive(GameObject prefab, int aIndex)
    {
        if (m_spawnedObjects.Contains (prefab))
        {
            GameObject objectToDespawn = m_spawnedObjects[aIndex];
            m_spawnableObjects.Add(objectToDespawn);
            m_spawnedObjects.RemoveAt(aIndex);
        }
    }

}
