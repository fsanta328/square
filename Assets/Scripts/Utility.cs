﻿using System.Collections;

public static class Utility 
{
	//fisher yates shuffle algorithm
	public static T[] ShuffleArray<T>(T[] a_array, int a_seed)
	{
		System.Random prng = new System.Random (a_seed);

		for (int i = 0; i < a_array.Length - 1; i++)
		{
			int randomIndex = prng.Next (i, a_array.Length);
			T tempItem = a_array [randomIndex];
			a_array [randomIndex] = a_array [i];
			a_array [i] = tempItem;
		}

		return a_array;
	}
}
