﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager m_Instance;
    public int m_logInPref;

	void Start ()
    {
        if (m_Instance != null)
        {
            Destroy(gameObject);
        }

        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
            m_logInPref = PlayerPrefs.GetInt("SignInPref");
        }
    }
}
